pub const CF_TOKEN: &'static str = "tokens";
pub const CF_TOKEN_INDEX_VALUE: &'static str = "tokens:index:value";

pub const CF_HABITS_ID: &'static str = "habits:id";
pub const CF_HABITS_USERID: &'static str = "habits:user_id";
